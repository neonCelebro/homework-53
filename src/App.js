import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddTaskForm from './AddTaskForm/AddTaskForm.js';
import Task from './Task/Task.js';

class App extends Component {
  state = {
    tasks : [
      {textTask: 'купить корм для котов', id: '1516335501968', status: false},
      {textTask: 'закончить домашнее задание', id: '1516335584872', status: false}
    ],
    currentTask: 'New Task',
  };

  changeTaskText = (event) => this.setState({currentTask: event.target.value});

  clearTaskText = () => this.setState({currentTask: ''});

  returnText = () => this.setState({currentTask: 'New Task'});

  addNewTask = () => {
   let tasks = [...this.state.tasks];
   let newTask = {
     textTask: this.state.currentTask,
     id: Date.now(),
   };
   tasks.push(newTask);
   this.setState({tasks});
   this.returnText();
  }
  removeTask = (id) => {
    const index = this.state.tasks.findIndex(p => p.id === id);
    let tasks = [...this.state.tasks];
    tasks.splice(index, 1);
    this.setState({tasks});
    };
    redyTask = (id) => {
      const index = this.state.tasks.findIndex(p => p.id === id);
      let tasks = [...this.state.tasks];
      const newTask = {...tasks[index]};
      newTask.status = !newTask.status;
      tasks[index] = newTask;
      this.setState({tasks})
    }

    render() {
    return (
      <div className="App">
        <AddTaskForm
        currentTask={this.state.currentTask}
        changeTask={this.changeTaskText}
        focusTask={this.clearTaskText}
        addTask={this.addNewTask}/>
        {this.state.tasks.map((task) => {
          return <Task redy={task.status} remuve={() => this.removeTask(task.id)} status={() => this.redyTask(task.id)}  textTask={task.textTask} key={task.id} />
        })
      }
      </div>
    );
  }
}

export default App;
